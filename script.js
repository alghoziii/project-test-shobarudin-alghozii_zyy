$(document).ready(function () {
  var scrollTop = 0;
  $(window).scroll(function () {
    scrollTop = $(window).scrollTop();
    $(".navbar").toggleClass("fixed-top scroll", scrollTop > 56);
  });
});

$(document).ready(function () {
  $(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    $("#banner").css("top", -(scrollTop * 0.2) + "px");
  });
});

const App = {
  apiUrl: "https://suitmedia-backend.suitdev.com/api/ideas",
  currentPage: 1,
  itemsPerPage: 10,
  sortOption: "-published_at",
  postListElement: document.getElementById("post-list"),
  paginationElement: document.getElementById("pagination"),
};

// Dummy data for testing
const dummyData = {
  data: [
    {
      id: 1,
      type: "ideas",
      attributes: {
        title: "5 September 2022",
        description: "Kenali Tingkat influencers berdasarkan jumlah followers",
        small_image: [{ url: "https://placekitten.com/200/200" }],
        medium_image: [{ url: "https://placekitten.com/200/200" }],
      },
    },
    {
      id: 2,
      type: "ideas",
      attributes: {
        title: "5 September 2022",
        description: "Jangan Asal Pilih Influencer,Berikut Cara Menyusun Strategi Influencer",
        small_image: [{ url: "https://placekitten.com/110/110" }],
        medium_image: [{ url: "https://placekitten.com/220/220" }],
      },
    },
    {
      id: 3,
      type: "ideas",
      attributes: {
        title: "5 September 2022",
        description: "Jangan Asal Pilih Influencer,Berikut Cara Menyusun Strategi Influencer",
        small_image: [{ url: "https://placekitten.com/120/120" }],
        medium_image: [{ url: "https://placekitten.com/240/240" }],
      },
    },
    {
      id: 4,
      type: "ideas",
      attributes: {
        title: "5 September 2022",
        description: "Kenali Tingkat influencers berdasarkan jumlah followers",
        small_image: [{ url: "https://placekitten.com/130/130" }],
        medium_image: [{ url: "https://placekitten.com/260/260" }],
      },
    },
    {
      id: 5,
      type: "ideas",
      attributes: {
        title: "5 September 2022",
        description: "Kenali Tingkat influencers berdasarkan jumlah followers",
        small_image: [{ url: "https://placekitten.com/130/130" }],
        medium_image: [{ url: "https://placekitten.com/260/260" }],
      },
    },
    {
      id: 6,
      type: "ideas",
      attributes: {
        title: "5 September 2022",
        description: "Jangan Asal Pilih Influencer,Berikut Cara Menyusun Strategi Influencer",
        small_image: [{ url: "https://placekitten.com/130/130" }],
        medium_image: [{ url: "https://placekitten.com/260/260" }],
      },
    },
    {
      id: 7,
      type: "ideas",
      attributes: {
        title: "5 September 2022",
        description: "Kenali Tingkat influencers berdasarkan jumlah followers",
        small_image: [{ url: "https://placekitten.com/130/130" }],
        medium_image: [{ url: "https://placekitten.com/260/260" }],
      },
    },
    {
      id: 8,
      type: "ideas",
      attributes: {
        title: " September 2022",
        description: "Kenali Tingkat influencers berdasarkan jumlah followers",
        small_image: [{ url: "https://placekitten.com/130/130" }],
        medium_image: [{ url: "https://placekitten.com/260/260" }],
      },
    },
  ],
  meta: {
    pageCount: 5,
  },
};

async function fetchData() {
  try {
    // Simulate an asynchronous fetch with a delay
    await new Promise((resolve) => setTimeout(resolve, 1000));

    console.log("Fetched dummy data:", dummyData);

    // Display dummy data
    displayData(dummyData.data, dummyData.meta);
  } catch (error) {
    console.error("Error fetching data:", error.message);
  }
}

function displayData(data, meta) {
  if (Array.isArray(data)) {
    data.forEach((post) => {
      const postCard = createPostCardHTML(post);
      App.postListElement.appendChild(postCard);
    });

    const totalPages = meta.pageCount || 0;
    updatePagination(totalPages);
  } else {
    console.error("Invalid data structure: 'data' should be an array.");
  }
}

function createPostCardHTML(post) {
  const postCard = document.createElement("div");
  postCard.className = "post-card";

  const attributes = post.attributes || {};
  const thumbnailUrl = attributes.small_image ? attributes.small_image[0]?.url || "" : "";
  const mediumImageUrl = attributes.medium_image ? attributes.medium_image[0]?.url || "" : "";
  const description = attributes.description || ""; // Add this line to get the description

  const imageElement = document.createElement("img");
  imageElement.src = mediumImageUrl || thumbnailUrl;
  imageElement.alt = attributes.title || "";
  imageElement.className = "post-thumbnail";

  const titleElement = document.createElement("div");
  titleElement.textContent = attributes.title || "";
  titleElement.className = "post-title";

  const descriptionElement = document.createElement("div");
  descriptionElement.innerHTML = `<strong>${description}</strong>`; // Use innerHTML to interpret HTML tags

  postCard.appendChild(imageElement);
  postCard.appendChild(titleElement);
  postCard.appendChild(descriptionElement); // Append the description element

  return postCard;
}

function clearPostList() {
  App.postListElement.innerHTML = "";
}

function updatePagination(totalPages) {
  App.paginationElement.innerHTML = "";
  for (let i = 1; i <= Math.min(totalPages, 5); i++) {
    const pageButton = document.createElement("button");
    pageButton.textContent = i;
    pageButton.addEventListener("click", () => updatePage(i));
    App.paginationElement.appendChild(pageButton);
  }
}

function updatePage(newPage) {
  App.currentPage = newPage;
  fetchData();
}

function updateItemsPerPage(newItemsPerPage) {
  App.itemsPerPage = newItemsPerPage;
  updatePage(1);
}

function updateSortOption(newSortOption) {
  App.sortOption = newSortOption;
  updatePage(1);
}

// Initial data fetch
fetchData();
